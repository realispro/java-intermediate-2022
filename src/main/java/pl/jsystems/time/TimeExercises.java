package pl.jsystems.time;

import java.time.*;
import java.time.chrono.ChronoPeriod;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;

public class TimeExercises {

    public static void main(String[] args) {

        //exercise1();
        //exercise2();
        //exercise3();
        //exercise4();
        exercise5();
    }


    public static void exercise1() {

        LocalDateTime present = LocalDateTime.now();
        LocalDateTime startOfCentury = LocalDateTime.of(2001, Month.JANUARY, 1, 00, 00);
        Duration daysGone = Duration.between(startOfCentury, present);
        System.out.println("days gone by " + (daysGone.toDays()));

        LocalDate today = LocalDate.now();
        LocalDate previousDate = LocalDate.of(2001, Month.JANUARY, 1);
        //.parse("2001-01-01");
        //Period period = Period.between(previousDate, today);
        long days = ChronoUnit.DAYS.between(previousDate, today);
        System.out.println("period: " + days);

        Duration difference = Duration.between(previousDate.atStartOfDay(), today.atStartOfDay());
        System.out.println("w XXI wieku minęło dni : " + difference.toDays());

    }


    public static void exercise2() {

        LocalDate today = LocalDate.now();
        YearMonth ym = YearMonth.of(today.getYear(), today.getMonth().getValue() + 1);
        String firstDay = ym.atDay(1).getDayOfWeek().name();
        System.out.println("Pierwszym dniem kolejnego miesiąca jest: " + firstDay);

        DayOfWeek dayOfWeek = today.plusMonths(1).withDayOfMonth(1).getDayOfWeek();
        System.out.println("dayOfWeek = " + dayOfWeek);
    }

    public static void exercise3() {
        LocalDate today = LocalDate.now();
        LocalDate lastDayOfCurrentMonth = today.plusMonths(1).withDayOfMonth(1).minusDays(1);
        //LocalDate lastDay = LocalDate.of(2022, Month.FEBRUARY,28);
        DayOfWeek dayOfWeek = DayOfWeek.from(lastDayOfCurrentMonth);
        System.out.println("dayOfWeek = " + dayOfWeek);
    }

    public static void exercise4() {
        LocalDateTime now5 = LocalDateTime.now();
        System.out.println(now5.plusDays(2));

        LocalDate friday = LocalDate.now().with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
        System.out.println("Nearest friday is on " + friday);
    }


    public static void exercise5() {
        LocalDate today = LocalDate.now();
        Month currentMonth = today.getMonth();

        ZonedDateTime timeInTokio = ZonedDateTime.now(ZoneId.of("Europe/Warsaw"))
                .withHour(16).withMinute(0).withZoneSameInstant(ZoneId.of("Asia/Tokyo"));

                /*
                ZonedDateTime.of(
                        LocalDate.of(2022, currentMonth, 16),
                        LocalTime.of(16, 0),
                        ZoneId.of("Europe/Warsaw"))
                .withZoneSameInstant(ZoneId.of("Asia/Tokyo"));*/

        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        System.out.println("5. Czas w Tokio = " + formatter.format(timeInTokio));

    }
}
