package pl.jsystems.time;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeMain {

    public static void main(String[] args) throws ParseException, InterruptedException {
        System.out.println("Let's check time");
        // 1.1.1970 0:00 GMT -> EPOC time

        Locale.setDefault(new Locale("pl", "PL"));

        long currentTimeMillis = System.currentTimeMillis();
        Date date = new Date();

        Calendar calendar = Calendar.getInstance();
        //calendar.setTime(date);
        calendar.set(2022, Calendar.FEBRUARY, 15, 0, 0, 0);
        calendar.add(Calendar.HOUR, -5);
        date = calendar.getTime();

        Locale locale = //Locale.ITALY;
                new Locale("pl", "PL");
        DateFormat format = new SimpleDateFormat("G MM'g'dd'g'YY X", locale);
                //DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.FULL, locale);

        String timestamp = format.format(date);
        System.out.println("timestamp = " + timestamp);
        
        /*String timeString = "00:12 AM";
        Date parsedDate = format.parse(timeString);
        System.out.println("parsedDate = " + parsedDate);*/

        // java 8: java.time
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        LocalDateTime localDateTime = LocalDateTime.of(2022, Month.FEBRUARY, 15, 14, 20, 0)
                .minusDays(5).plusHours(2);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("G MM'g'dd'g'YY hh:mm:ss a X", locale);
                //DateTimeFormatter.ISO_LOCAL_TIME;

        ZonedDateTime zonedDateTime = ZonedDateTime.now(ZoneId.of("Europe/Warsaw"));
        ZonedDateTime singaporeLanding = zonedDateTime.plusHours(7).withZoneSameInstant(ZoneId.of("Asia/Singapore"));

        LocalTime now = LocalTime.now();
        timestamp = formatter.format(singaporeLanding);
        System.out.println("timestamp2= " + timestamp);

        //Thread.sleep(1000);
        LocalTime now2 = LocalTime.now();

        System.out.println("is before ? " + now.isBefore(now2));

        Duration duration = Duration.between(now, now2);
        System.out.println("nanos duration: " + duration.toNanos());


    }
}
