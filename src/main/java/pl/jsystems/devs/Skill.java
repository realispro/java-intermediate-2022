package pl.jsystems.devs;

public enum Skill {

    JAVA(true),
    PYTHON(true),
    REACT_JS(false),
    ANGULAR(false),
    JAVASCRIPT(false),
    SQL(true),
    JPA(true),
    SPRING(true),
    ESB(true);

    private boolean backend;

    private Skill(boolean backend){
        this.backend = backend;
    }

    public boolean isBackend() {
        return backend;
    }
}
