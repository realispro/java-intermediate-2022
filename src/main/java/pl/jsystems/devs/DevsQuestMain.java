package pl.jsystems.devs;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class DevsQuestMain {

    public static void main(String[] args) {

        List<Developer> devs = new ArrayList<>(
                List.of(
                        new Developer("Robert","Python")
                                .withSkill(Skill.JAVA, 5).withSkill(Skill.PYTHON, 3),
                        new Developer("Robert","Data")
                                .withSkill(Skill.SQL, 3).withSkill(Skill.JAVA, 8)
                                .withSkill(Skill.SPRING, 4).withSkill(Skill.ANGULAR, 2),
                        new Developer("Robert","Java")
                                .withSkill(Skill.JAVA, 2).withSkill(Skill.JAVASCRIPT, 7)
                                .withSkill(Skill.PYTHON, 4),
                        new Developer("Robert","Front").withSkill(Skill.JAVASCRIPT, 9)
                )
        );
        System.out.println("let's resolve TODOs!");
        // TODO 1: list all devs last names !!!
        // TODO 2: find all devs having JAVA skill !!!
        // TODO 3: sum of experience for each dev ???
        // TODO 4: avg experience in JAVA of a team ***
        // TODO 5: count all team skills !!!


        todo4(devs);

    }


    public static void todo1(List<Developer> team){
        // opt1
        team.stream()
                .map(Developer::getLastName)// developer -> developer.getLastName()
                .forEach(System.out::println); // x -> System.out.println(x)

        // opt2
        team.forEach(d-> System.out.println(d.getLastName()));

        // opt3
        List<String> devsLastNames = new ArrayList<>();
        team.forEach(developer -> devsLastNames.add(developer.getLastName()));
        System.out.println("devsLastNames = " + devsLastNames);

        // opt4
        List<String> devsLastNames2 = team.stream()
                .map(developer -> developer.getLastName())
                .collect(Collectors.toList());
    }


    public static void todo2(List<Developer> team){

        var result2= team.stream()
                .filter(d->(d.skills.stream().anyMatch(p->p.getFirst()==Skill.JAVA)))
                //.count();
                .collect(Collectors.toList());
        System.out.println("result = " + result2);
    }

    public static void todo4(List<Developer> team){
        OptionalDouble optionalAvg = 
                team.stream()
                .filter(d -> d.getSkills().stream().anyMatch(p -> p.getFirst() == Skill.JAVA))
                .flatMap(d -> d.getSkills().stream().filter(p->p.getFirst()==Skill.JAVA).map(p->p.getSecond()))
                        .mapToInt(i -> i.intValue())
                                .average();

        System.out.println("optionalAvg.getAsDouble() = " + optionalAvg.getAsDouble());
    }


    public static void todo5(List<Developer> team){
        long count = team.stream()
                .flatMap(d -> d.getSkills().stream().map(p -> p.getFirst()))
                .distinct()
                .count();
        System.out.println("count = " + count);

    }

}
