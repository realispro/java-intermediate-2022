package pl.jsystems.devs;

import java.util.*;

public final class Developer {

    private final String firstName;
    private String lastName;

    public Set<Pair<Skill,Integer>> skills = new HashSet<>();

    public Developer(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public void addSkill(Skill skill) throws IllegalStateException{
        withSkill(skill, 1);
    }

    public Developer withSkill(Skill skill, int experience){
        skills.add(new Pair<>(skill, experience));
        return this;
    }

    public void removeSkills(Skill... skills){
        for (Skill skill: skills) {
            this.skills.remove(skill);
        }
    }

    public void removeSkill(Skill skill){
        /*for(Skill s : skills){
            if(s==skill){
                skills.remove(s);
            }
        }*/
        Iterator<Pair<Skill, Integer>> itr = skills.iterator();
        while(itr.hasNext()){
            Pair<Skill, Integer> pair = itr.next();
            if(pair.getFirst()==skill){
                itr.remove();
            }
        }
    }

    public Developer withSkills(Skill... skills) {
        for(Skill skill: skills){
            this.addSkill(skill);
        }
        return this;
    }


    public Set<Pair<Skill,Integer>> getSkills() {
        return Collections.unmodifiableSet(skills);
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Developer developer = (Developer) o;
        return Objects.equals(firstName, developer.firstName) && Objects.equals(lastName, developer.lastName) && Objects.equals(getSkills(), developer.getSkills());
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, getSkills());
    }

    @Override
    public String toString() {
        return "Developer{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", skills=" + skills +
                '}';
    }
}
