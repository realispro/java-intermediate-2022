package pl.jsystems;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class I18NMain {

    public static void main(String[] args) {
        System.out.println("Let's localize!");

        String winner = "Kubacki";
        String discipline = "skijumping";

        Locale.setDefault(new Locale("pl", "PL"));

        Locale locale = new Locale("en", "US");
        // messages_en_US
        // messages_en
        // messages_pl_PL
        // messages_pl
        // messages
        ResourceBundle bundle = ResourceBundle.getBundle("olimpics.messages", locale);

        String announcementPattern = bundle.getString("winner.announcement");
        System.out.println("announcementPattern = " + announcementPattern);

        String announcement = MessageFormat.format(announcementPattern, discipline, winner);
        System.out.println("announcement = " + announcement);



    }
}
