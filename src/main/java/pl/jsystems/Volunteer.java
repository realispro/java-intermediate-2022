package pl.jsystems;

import java.util.Random;

public class Volunteer {

    public static void main(String[] args) {
        System.out.println("Let's choose volunteer ;)");
        int candidatesCount = 12;
        int chosen = new Random(System.currentTimeMillis()).nextInt(candidatesCount) + 1;
        System.out.printf("Candidate number %d has been chosen", chosen);
    }
}
