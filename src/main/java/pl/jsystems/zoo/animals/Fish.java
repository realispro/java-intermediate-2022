package pl.jsystems.zoo.animals;

import pl.jsystems.zoo.Animal;

public abstract class Fish extends Animal {

    public Fish(String name, int size) {
        super(name, size);
    }

    @Override
    public final void move() {
        System.out.println(name + " is swimming...");
    }
}
