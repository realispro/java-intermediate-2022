package pl.jsystems.zoo.animals;

import pl.jsystems.zoo.Animal;

public class Oyster extends Animal {

    private static String staticResource = "some static resource";

    private String resource = "some resource";

    public Oyster(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println("Oyster is barely moving");
    }


    public static class Pearl{

        private int diameter;

        public Pearl(int diameter) {
            this.diameter = diameter;
            System.out.println("constructing pearl of diameter " + diameter + /*" using resource '"
                    + resource +*/ "' and static resource '" + staticResource + "'" );
        }

        public int getDiameter() {
            return diameter;
        }
    }
}
