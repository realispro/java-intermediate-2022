package pl.jsystems.zoo.animals;


import pl.jsystems.zoo.Animal;

public abstract class Bird extends Animal {

    public Bird(String name, int size){
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println(this.name + " is flying...");
    }
}
