package pl.jsystems.zoo.animals;

import pl.jsystems.zoo.Animal;

public abstract class Mammal extends Animal {
    public Mammal(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println(this.name + " is walking...");
    }
}
