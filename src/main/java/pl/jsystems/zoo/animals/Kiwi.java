package pl.jsystems.zoo.animals;

public class Kiwi extends Bird{
    public Kiwi(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println(name + " is walking...");
    }
}
