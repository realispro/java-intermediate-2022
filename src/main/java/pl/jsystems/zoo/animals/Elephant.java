package pl.jsystems.zoo.animals;

public class Elephant extends Mammal{
    public Elephant(String name, int size) {
        super(name, size);
    }
}
