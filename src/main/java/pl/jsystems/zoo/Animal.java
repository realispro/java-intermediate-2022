package pl.jsystems.zoo;

import java.util.Objects;

public abstract class Animal implements Comparable<Animal>{

    protected String name;
    protected int size;

    public Animal(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public void eat(String food){
        System.out.println(name + " is consuming " + food);
    }

    public abstract void move();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }


    @Override
    public int compareTo(Animal a) {
        return this.size - a.size;
    }

    @Override
    public boolean equals(Object o) {
        System.out.println("comparing " + this + " with " + o);
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return getSize() == animal.getSize() && Objects.equals(getName(), animal.getName());
    }

    @Override
    public int hashCode() {
        int hash = Objects.hash(getClass(), getName(), getSize());
        System.out.println("hash of " + this + " is " + hash);
        return hash;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }
}
