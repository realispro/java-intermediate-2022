package pl.jsystems.zoo;


import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import pl.jsystems.zoo.animals.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;

public class ZooMain {


    public static void main(String[] args) {

        Bird kiwi = new Kiwi("Jimmi", 5);
        Bird eagle = new Eagle("Bielik", 15);
        Animal animal = new Elephant("Jimmi", 500);

        List<Animal> animals = new ArrayList<>();
                // new HashSet<>();
        animals.add(animal);
        animals.add(kiwi);
        animals.add(eagle);
        animals.add(kiwi);

        System.out.println("animal at position 2: " + animals.get(animals.size()-1));

        Comparator<Animal> comparator = (a1, a2) -> a1.getSize()-a2.getSize();

                /*new Comparator<>() {
            @Override
            public int compare(Animal a1, Animal a2) {
                return a1.getSize()-a2.getSize();
            }
        };*/
                //new AnimalComparator();

        Collections.sort(animals, comparator);


        Set<Animal> animalSet = new TreeSet<>(comparator);
        animalSet.addAll(animals);
                //new HashSet<>(animals);

        Animal elephant = new Eagle("Jimmi", 500);
        if(animalSet.contains(elephant)){
            System.out.println("Jimmi still in a zoo!");
        }

        Iterator<Animal> itr = animals.iterator();
        while (itr.hasNext()){
            Animal a = itr.next();
            if(!(a instanceof Bird)){
                itr.remove();
            }
        }

        for (Animal a : animalSet) {
            System.out.println("a = " + a);
        }

        System.out.println("There is " + animals.size() + " animals in a zoo");

        BiMap<Month, Animal> stars = HashBiMap.create();
                //new LinkedHashMap<>();
        stars.put(Month.JANUARY, kiwi);
        stars.put(Month.FEBRUARY, eagle);
        //stars.put(Month.MARCH, kiwi);
        stars.put(Month.APRIL, null);

        Month currentMonth = LocalDate.now().getMonth();
        Animal star = stars.get(currentMonth);
        System.out.println("star = " + star);

        //stars.remove(Month.APRIL);

        for(Map.Entry<Month, Animal> entry : stars.entrySet()){
            System.out.println("key:" + entry.getKey() + " value: " + entry.getValue());
        }

        BiMap<Animal, Month> inversed = stars.inverse();
        Month kiwiMonth = inversed.get(kiwi);
        System.out.println("kiwiMonth = " + kiwiMonth);




        Map<Integer, String> strings = new HashMap<>();
        strings.put(1, "1");
        strings.put(Integer.valueOf(2), "2"); //autoboxing
        strings.put(3, " ");
        strings.put(4, null);

        String s = strings.get(3);
        if(s.trim().isEmpty()){
            System.out.println("s is empty");
        }



    }




}
