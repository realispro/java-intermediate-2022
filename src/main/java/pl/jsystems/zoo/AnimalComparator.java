package pl.jsystems.zoo;

import java.util.Comparator;

public class AnimalComparator implements Comparator<Animal> {

    @Override
    public int compare(Animal a1, Animal a2) {
        int nameResult = a1.getName().compareTo(a2.getName());
        if(nameResult==0){
            return a1.getSize() - a2.getSize();
        }
        return nameResult;
    }
}
