package pl.jsystems.zoo;

import pl.jsystems.zoo.animals.Oyster;

public class InnerMain {

    public static void main(String[] args) {

        Oyster oyster = new Oyster("Yummy", 2);
        Oyster.Pearl pearl = new Oyster.Pearl(2);
        Oyster.Pearl pearl2 = new Oyster.Pearl(2);
        Oyster.Pearl pearl3 = new Oyster.Pearl(2);
        oyster = null;
    }
}
