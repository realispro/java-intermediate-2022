package pl.jsystems.io;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalTime;

public class IOMain {

    public static void main(String[] args) /*throws IOException*/ {

        File dir = new File("mydir");

        if(dir.exists()){
            System.out.println("directory exists...");
            FilenameFilter filter = (dir1, name) -> name.endsWith(".txt");
            String[] fileNames = dir.list(filter);
            for(String fileName : fileNames){
                System.out.println("file:" + fileName);
            }
        } else {
            System.out.println("directory is missing. creating one...");
            dir.mkdirs();
        }

        File file = new File(dir,"time.txt");
        if(file.exists()){
            System.out.println("File found.");
        } else {
            System.out.println("File missing. Creating one...");
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try( BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));) {
            for (int i = 0; i < 3; i++) {
                bw.write(LocalTime.now().toString());
                bw.newLine();
            }
            bw.newLine();
        } catch (IOException e){
            e.printStackTrace();
        }

        try(BufferedReader br = new BufferedReader(new FileReader(file))) {

            String line;
            while((line=br.readLine())!=null){
                System.out.println("line:" + line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Files.lines(Paths.get("mydir","time.txt")).forEach(line->System.out.println("stream:" + line));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
