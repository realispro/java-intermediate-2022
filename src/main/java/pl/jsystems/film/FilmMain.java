package pl.jsystems.film;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class FilmMain {

    public static void main(String[] args) {

        Director kiepski = new Director();
        kiepski.setFirstName("Ferdynand");
        kiepski.setLastName("Kiepski");

        //addDirector(kiepski);

        List<Director> directors = getDirectorsJPA();
        directors.forEach(System.out::println);

        List<Movie> movies = getMoviesJPA();
        movies.forEach(System.out::println);

    }

    // JPA - EntityManager
    private static List<Director> getDirectorsJPA() {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("filmweb");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("select d from Director d");
        return query.getResultList();
    }

    private static List<Movie> getMoviesJPA() {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("filmweb");
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("select m from Movie m");
        return query.getResultList();
    }



    // JDBC - Connection

    private static List<Director> getDirectors() {

        List<Director> directors = new ArrayList<>();

        try (Connection connection = getConnection();) {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery("select id, first_name, last_name from director");
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                Director director = new Director();
                director.setId(id);
                director.setFirstName(firstName);
                director.setLastName(lastName);
                directors.add(director);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return directors;
    }

    private static void addDirector(Director director) {

        try (Connection connection = getConnection()) {
            connection.setAutoCommit(false);
            String insertSql = "INSERT INTO DIRECTOR(FIRST_NAME, LAST_NAME) VALUES(?,?)";
            PreparedStatement stmt = connection.prepareStatement(insertSql);
            stmt.setString(1, director.getFirstName());
            stmt.setString(2, director.getLastName());

            int rows = stmt.executeUpdate();
            System.out.println("rows updated = " + rows);
            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private static Connection getConnection() {

        try {
            Properties props = new Properties();
            props.load(FilmMain.class.getResourceAsStream("/jdbc.properties"));

            String driverClassName = props.getProperty("jdbc.driver");
            String url = props.getProperty("jdbc.url");
            String user = props.getProperty("jdbc.user");
            String password = props.getProperty("jdbc.password");

            Class.forName(driverClassName);
            return DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
