package pl.jsystems.holiday;

@FunctionalInterface
public interface Transportation {

    void transport(String passenger);

    default int getSpeed(){
        return -1;
    }

    static String getDescription(){
        return "some random description";
    }

}
