package pl.jsystems;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FirstClass {

    public static void main(String[] args){
        System.out.println("Happy Valentine Day!");

        byte b = 12; // 1 bajt -128 +127
        short s = 12; // 2 bajt -32688 +32687
        int i = 12; // 4 bajt
        long l = 1245435333L; // 8 bajt

        float f = 12.3F; // 4 bajt
        double d = 12.3; // 8 bajt

        boolean bool = true; // 1 bit
        char c = '\t'; // 2 bajt

        String s1 = "Hey Joe";
        String s2 = new String("Hey Joe");
        String s3 = "Hey Joe";


        System.out.println(s1==s2);
        System.out.println(s1.equals(s2));
        System.out.println(s1==s3);
        System.out.println(s1.equals(s3));
        System.out.println(s2==s3);
        System.out.println(s2.equals(s3));

        String address = "00-950 Warszawa, Krakow 30 520";
        String patternText = "\\d{2}[-\\s]\\d{3}";

        Pattern p = Pattern.compile(patternText);
        Matcher m = p.matcher(address);

        while(m.find()){
            System.out.println("found: " + m.group() + " at position " + m.start());
        }

        String formatted = String.format("Parametrized message, param1 %2$s param2 %1$d", 123, "Hey!");
        System.out.printf("Parametrized message, param1 %2$s param2 %1$d", 123, "Hey!");


    }

}
