package pl.jsystems.stream;

import pl.jsystems.zoo.Animal;
import pl.jsystems.zoo.animals.Eagle;
import pl.jsystems.zoo.animals.Elephant;
import pl.jsystems.zoo.animals.Fish;
import pl.jsystems.zoo.animals.Kiwi;

import java.time.LocalTime;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamMain {

    public static void main(String[] args) {
        System.out.println("Let's process objects");

        Kiwi joe = new Kiwi("Joe", 60);

        List<Animal> animals = new LinkedList<>();
        animals.add(new Eagle("George", 40));
        //animals.add(new Elephant("Bonifacy", 600));
        animals.add(joe);
        animals.add(new Elephant("Euzebiusz", 450));
        animals.add(new Eagle("Bielik", 50));
        animals.add(joe);

        // TODO sorting, filtering, mapping, searching

        // animal names (no duplicates) lighter then 500 units sorted by size
        var result =
                animals.stream()
                        .sorted((a1, a2) -> a1.getSize() - a2.getSize())
                        .filter(a -> a.getSize() < 500)
                        .map(a -> a.getName())
                        .distinct()
                        .collect(Collectors.toList());
        //.count();

        System.out.println("result = " + result);

        // Map of animals with their names as keys
        Map<String, Animal> animalMap = animals.stream().distinct().collect(Collectors.toMap(
                a -> a.getName(),
                a -> a
        ));
        System.out.println("animalMap = " + animalMap);

        // map of animals sorted by their size
        Map<String, Animal> animalMapSorted =
                animalMap.entrySet().stream()
                        .sorted((e1, e2) -> e1.getValue().getSize() - e2.getValue().getSize())
                        .collect(Collectors.toMap(
                                e -> e.getKey(),
                                e -> e.getValue(),
                                (e1, e2) -> e1, // ignore please
                                () -> new LinkedHashMap<>()));

        System.out.println("animalMapSorted = " + animalMapSorted);

        // find any fish
        Optional<Animal> optionalAnimal = animals.stream()
                .filter(a -> a instanceof Fish)
                .findAny();
        System.out.println("found:" + optionalAnimal.isPresent() + " " + optionalAnimal.orElse(null));


        // animal name contains 'a'
        boolean match = animals.stream()
                //        .noneMatch( a -> a.getName().contains("a"));
                .allMatch(a -> a.getName().matches("^[A-Z][a-z]*"));

        System.out.println("match = " + match);

        int limit = LocalTime.now().getSecond();

        Stream<String> stringsStream = Stream.generate( () -> "o");
        stringsStream.limit(limit).forEach(s->System.out.println("s:" + s));

        // 1111 1111 + 1111 1111 = 0000 0001 0000 0000
        Stream<Byte> intStream = Stream.iterate((byte)1, (Byte i)-> (byte)(i + 1));
        byte sum = intStream
                .limit(5)
                .reduce((byte)0, (i1, i2)->(byte)(i1+i2));
                //.forEach(i->System.out.println("i:" + i));
        System.out.println("sum = " + sum);

    }

}
