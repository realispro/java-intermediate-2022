package pl.jsystems.calculator;

import java.util.Arrays;

public class ArrayMemory implements Memory{

    private double[] values = new double[5]; //{0, 0, 0, 0, 0}


    @Override
    public double getValue() {
        return values[values.length-1];
    }

    // {0,1,2,3,4} + 5, index: 4
    // v1: {5,1,2,3,4} index: 0
    // v2: {1,2,3,4,5} index: 4
    // v3: {4,3,2,1,0} + 5 -> {5,4,3,2,1} index: 0
    @Override
    public void setValue(double value) {
        System.arraycopy(values, 1, values, 0, values.length-1 );
        values[values.length-1] = value;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        for( double value : values ){
            builder.insert(0, value).insert(0, ",");
        }

        return "ArrayMemory{" +
                "values=" + builder.toString() +
                '}';
    }
}
