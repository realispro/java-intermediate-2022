package pl.jsystems.calculator;

public interface Memory {

    double getValue();

    void setValue(double value);

}
