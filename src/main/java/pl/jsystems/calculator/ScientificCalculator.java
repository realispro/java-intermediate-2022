package pl.jsystems.calculator;

public class ScientificCalculator extends Calculator {

    public ScientificCalculator(){
        this(0);
    }

    public ScientificCalculator(double result){
        super(result);
    }

    @Override
    public String getProducer() {
        return "Texas Instruments";
    }


    public double power(int operand){
        memory.setValue(Math.pow(memory.getValue(), operand));
        return memory.getValue();
    }

    @Override
    public double divide(double operand) {
        if(operand==0){
            System.out.println("WARNING pamietaj ch...o nie dziel przez zero");
        }
        return super.divide(operand);
    }
}
