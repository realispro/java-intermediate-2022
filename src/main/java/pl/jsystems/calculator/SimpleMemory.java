package pl.jsystems.calculator;

public class SimpleMemory implements Memory{

    private double value;

    @Override
    public double getValue() {
        return value;
    }

    @Override
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "SimpleMemory{" +
                "value=" + value +
                '}';
    }
}
