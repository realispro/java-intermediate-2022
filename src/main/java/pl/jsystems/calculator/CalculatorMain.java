package pl.jsystems.calculator;

import static pl.jsystems.calculator.Calculator.multiply;

public class CalculatorMain {

    public static void main(String[] args) { // psvm or main
        System.out.println("Let's calculate!"); // sout
        double result = multiply(5000, 0.30);
        System.out.println("result = " + result); // soutv

        Calculator calc1 = provideCalulator(0);
        final Calculator calc2 = provideCalulator(0);

        calc1.add(2);
        calc1.add(2);
        calc1.add(2);
        calc1.add(2);
        calc1.add(2);
        calc1.add(2);

        calc2.divide(0);
        //calc2.power(3);
        System.out.println("calculated=" + calc2.getResult());
        System.out.println("calc1=" + calc1);
    }

    public static Calculator provideCalulator(double initial){
        return new ScientificCalculator(initial);
    }




    /*
    public static double multiply(double operand1, double operand2) {
        return operand1 * operand2;
    }*/

}
