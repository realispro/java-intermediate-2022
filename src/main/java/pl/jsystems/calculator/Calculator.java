package pl.jsystems.calculator;

public abstract class Calculator /*extends Object*/ {

    protected Memory memory = new ArrayMemory();

    public Calculator(double result) {
        this.memory.setValue(result);
    }

    public Calculator(Memory memory){
        this.memory = memory;
    }

   /* public Calculator(){
        this(0.0);
        System.out.println("default constructor");
    }*/

    public abstract String getProducer();

    public double add( double operand ){
        memory.setValue(memory.getValue()+operand);
        return memory.getValue();
    }

    public double subtract( double operand ){
        memory.setValue(memory.getValue()-operand);
        return memory.getValue();
    }

    public final double multiply( double operand ){
        memory.setValue(memory.getValue()*operand);
        return memory.getValue();
    }

    public double divide( double operand ){
        memory.setValue(memory.getValue()/operand);
        return memory.getValue();
    }

    public double getResult() {
        return memory.getValue();
    }

    public static double multiply(double operand1, double operand2) {
        return operand1 * operand2;
    }

    @Override
    public String toString() {
        return "Calculator{" +
                "memory=" + memory +
                '}';
    }
}
